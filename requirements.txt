astropy-healpix
pygcn
healpy
ligo.skymap
seaborn
sphinx
sphinx-tabs >= 1.1.11
